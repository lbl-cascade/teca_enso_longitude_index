""" Calculte the ENSO Longitude Index (ELI) following Williams and Patricola (2018, GRL). 

Written by Travis A. O'Brien <TAOBrien@lbl.gov>
Copyright University of California (2019).  All Rights Reserved.

"""

import numpy as np


def calculate_enso_longitude_index(sst,
                                   lat2d,
                                   lon2d,
                                   pacific_lon_min=115,
                                   pacific_lon_max=290,
                                   tropical_latitude=5,
                                   weight_by_lat=True):
    """ Calculates the ENSO Longitude Index (ELI) following Williams and Patricola (2018, GRL).

        input:
        ------

            sst                : the timeseries of SST data 
                                 It is assumed that land values are masked out and that the mask 
                                 does not depend on time.
                                 (dimensions should be [time, lat, lon])

            lat2d              : the latitude field.
                                 Values should be in [-90,90].
                                 (dimensions should be [lat, lon])

            lon2d              : the longitude field
                                 Values should be in [0,360].
                                 (dimensions should be [lat, lon])

            pacific_lon_min    : the minimum longitude of values to be considered as part of the Pacific

            pacific_lon_max    : the maximum longitude of values to be considered as part of the Pacific

            tropical_latitude  : the maximum latitude (N and S) considered to be part of the tropics

            weight_by_lat      : flags whether to weight averages by latitude

        output:
        -------

            eli            : the ELI field (dimensions [time])

        raises:
        -------

            ValueError if any values are outside the expected range

            RuntimeError if any of the arrays have an inconsistent or unexpected shape

            TypeError if any of the arrays don't behave like numpy ndarrays


        This follows the algorithm outlined in Williams and Patricola (2018, GRL).  For each timestep:

          1. Calculate the tropical mean SST (between +/- 5 degrees N)

          2. Find grid cells in the Pacific ocean that are warmer than the tropical mean SST

          3. Calculate the average longitude of these cells that are warmer than the tropical mean; this is the ELI for that timestep

        "ELI is calculated by first, for each month, calculating the tropical-average SST over 5°S–5°N, to estimate the
         SST threshold for convection. We then create a binary spatial mask, assigning 1 to points where SST is at least
         the threshold value and 0 to points where SST is less than the threshold. Finally, ELI is the average of all longitudes
         over which this spatial mask is 1, within the Pacific basin and over 5°S–5°N. The results are insensitive
         to replacing 5°S–5°N with 20°S–20°N. ELI is not weighted by how much SST exceeds the threshold." (Williams and Patricola, 2018)


        Written by Travis A. O'Brien <TAOBrien@lbl.gov>
        Copyright University of California (2019).  All Rights Reserved.

    """

    # do some duck type checking on the input values
    for varname, field in zip(["sst", "lat2d", "lon2d"], [sst, lat2d, lon2d]):
        try:
            np.asarray(field)
            field.shape
        except:
            raise TypeError(
                "casting `{}` as a numpy array failed; is it a numpy-like array?".format(varname))

    try:
        pacific_lon_max < 0 or pacific_lon_max > 360
    except:
        raise ValueError(
            "`pacific_lon_max is not behaving like a scalar value; is an array inadvertently being passed in?")
    try:
        pacific_lon_min < 0 or pacific_lon_min > 360
    except:
        raise ValueError(
            "`pacific_lon_min is not behaving like a scalar value; is an array inadvertently being passed in?")

    # check that arrays have the expected ranks
    if len(sst.shape) != 3:
        raise RuntimeError(
            "sst has rank {}, but a rank of 3 is required".format(len(sst.shape)))
    if len(lat2d.shape) != 2:
        raise RuntimeError(
            "lat2d has rank {}, but a rank of 2 is required".format(len(lat2d.shape)))
    if len(lon2d.shape) != 2:
        raise RuntimeError(
            "lon2d has rank {}, but a rank of 2 is required".format(len(lon2d.shape)))

    # check that array shapes are consistent
    ntime, nlat, nlon = sst.shape
    for varname, field in zip(["lat2d", "lon2d"], [lat2d, lon2d]):
        if field.shape[0] != nlat or field.shape[1] != nlon:
            raise RuntimeError("`{}` has shape {}, but a shape of [{},{}] is required.".format(
                varname, field.shape, nlat, nlon))

    # check that lat/lon values are in the expected range
    if lat2d.min() < -90 or lat2d.max() > 90:
        raise ValueError(
            "`lat2d` has values in the range [{},{}], but it's values should be in the range [-90,90]".format(lat2d.min(), lat2d.max()))
    if lon2d.min() < 0 or lon2d.max() > 360:
        raise ValueError("`lon2d` has values in the range [{},{}], but it's values should be in the range [0,360]".format(
            lon2d.min(), lon2d.max()))

    # check that pacific_lon_min and pacific_lon_max are within range
    if pacific_lon_max < 0 or pacific_lon_max > 360:
        raise ValueError(
            "`pacific_lon_max = {}, but it should be in the range [0,360]".format(pacific_lon_max))
    if pacific_lon_min < 0 or pacific_lon_min > 360:
        raise ValueError(
            "`pacific_lon_min = {}, but it should be in the range [0,360]".format(pacific_lon_min))
    if pacific_lon_min >= pacific_lon_max:
        raise ValueError("`pacific_lon_min` should be smaller than `pacific_lon_max`: pacific_lon_min = {} >= pacific_lon_max = {}".format(
            pacific_lon_min, pacific_lon_max))

    if weight_by_lat:
        # generate weights and mask them using the 1st timestep of the SST field
        weight2d = np.ma.masked_where(np.ma.getmaskarray(
            sst[0, :, :]), np.ma.cos(lat2d*np.pi/180))

    # ***************************************************************
    #  1. Calculate the tropical mean SST (between +/- 5 degrees N)
    # ***************************************************************
    # create an SST array that is masked outside the tropics (beyond 5 degrees N/S)
    sst_tropics_only = np.ma.masked_where(np.abs(lat2d[np.newaxis, :, :])*np.ma.ones(sst.shape) > tropical_latitude,
                                          sst)
    # do the average
    if not weight_by_lat:
        sst_tropical_mean = sst_tropics_only.mean(axis=(1, 2))
    else:
        # mask weights in the same way as the SSTs
        weight3d_tropics_only = np.ma.masked_where(np.abs(lat2d[np.newaxis, :, :])*np.ma.ones(sst.shape) > tropical_latitude,
                                                   weight2d[np.newaxis, :, :]*np.ma.ones(sst.shape))
        # do a weighted average (a normalied, weighted sum over the lon (axis=2) and lat (axis=1) dimensions.)
        sst_tropical_mean = (weight3d_tropics_only*sst_tropics_only).sum(
            axis=2).sum(axis=1)/weight3d_tropics_only.sum(axis=2).sum(axis=1)

    # **********************************************************
    # 2. Find grid cells in the Pacific ocean that are warmer
    #    than the tropical mean SST (mask values colder)
    # **********************************************************

    lon2d_pacific_only = np.ma.masked_greater(lon2d, pacific_lon_max)
    lon2d_pacific_only = np.ma.masked_less(lon2d_pacific_only, pacific_lon_min)
    lon2d_pacific_only = np.ma.masked_where(
        np.abs(lat2d) > tropical_latitude, lon2d_pacific_only)

    # create a 3D pacific longitude array that is masked where SSTs are less than the tropical mean
    lon3d_pacific_only_sst_masked = np.ma.masked_where(sst < sst_tropical_mean[:, np.newaxis, np.newaxis],
                                                       lon2d_pacific_only[np.newaxis, :, :]*np.ma.ones(sst.shape))

    # ***********************************************************
    # 3. Calculate the average longitude of these cells that
    #    are warmer than the tropical mean; this is the ELI for
    #    that timestep
    # ***********************************************************
    if not weight_by_lat:
        eli = lon3d_pacific_only_sst_masked.mean(axis=(1, 2))
    else:
        # mask weights in the same way as the longitudes
        weight2d_pacific_only = np.ma.masked_where(
            np.ma.getmaskarray(lon2d_pacific_only), weight2d)
        weight3d_pacific_only_sst_masked = np.ma.masked_where(sst < sst_tropical_mean[:, np.newaxis, np.newaxis],
                                                              weight2d_pacific_only[np.newaxis, :, :]*np.ma.ones(sst.shape))

        # do a weighted average (a normalied, weighted sum over the lon (axis=2) and lat (axis=1) dimensions.)
        eli = (weight3d_pacific_only_sst_masked*lon3d_pacific_only_sst_masked).sum(
            axis=2).sum(axis=1)/weight3d_pacific_only_sst_masked.sum(axis=2).sum(axis=1)

    return eli
